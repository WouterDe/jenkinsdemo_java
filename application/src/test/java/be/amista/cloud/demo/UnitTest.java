package be.amista.cloud.demo;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import be.amista.cloud.helpers.MaterialHelper;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.*;
import java.nio.file.Paths;
import java.util.Iterator;

public class UnitTest {

    @Test
    /**
     * Checks if the json file is available
     */
    public void shouldFindJSONFile() {
	    	try {
	    		ClassLoader classLoader = this.getClass().getClassLoader();
	        File file = new File(classLoader.getResource("materials.json").getFile());
	        assertTrue(file.exists());
	     }
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    		assertTrue(false);
	    	}
    }
    
    @Test
    /**
     * Checks if the helper method returns an array with more than 1 object
     */
    public void shouldReturnMaterials() {
	    	try {
	    		ClassLoader classLoader = this.getClass().getClassLoader();
	        File file = new File(classLoader.getResource("materials.json").getFile());
	        String filePath = file.getAbsolutePath();
	        
	        JSONArray materials = MaterialHelper.getAllMaterialsFromFile(filePath);
	        assertTrue(materials.length() > 1);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    		assertTrue(false);
	    	}
    }
    
    @Test
    /**
     * check if the converted objects contain all expected properties
     */
    public void hasAllProperties() {
	    	try {
	    		boolean propertiesFound = true;
	    		ClassLoader classLoader = this.getClass().getClassLoader();
	        File file = new File(classLoader.getResource("materials.json").getFile());
	        String filePath = file.getAbsolutePath();
	        
	        JSONArray materials = MaterialHelper.getAllMaterialsFromFile(filePath);
	        JSONArray exportMaterials = MaterialHelper.convertMaterialsForExporting(materials);
	        
	        Iterator<Object> iterator = exportMaterials.iterator();
	        while(iterator.hasNext()) {
	        		JSONObject material = (JSONObject) iterator.next();
	        		
	        		assertTrue("Property generatedId is missing",material.has("GeneratedId"));
	        		assertTrue("Property Material is missing",material.has("Material"));
	        		assertTrue("Property MaterialName is missing",material.has("MaterialName"));
	        }
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    		assertTrue(false);
	    	}
    }
}
