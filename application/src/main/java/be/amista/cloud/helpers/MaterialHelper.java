package be.amista.cloud.helpers;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonParser;

public class MaterialHelper {
	//private static final List<String> exportingProperties = Arrays.asList("GeneratedId", "Material", "MaterialName");
	private static final List<String> exportingProperties = Arrays.asList("GeneratedId", "Material", "PlantName");

	public static JSONArray getAllMaterialsFromFileStream(FileInputStream fileInputStream) throws Exception {
		JSONArray materials = new JSONArray();
		
		JsonParser jsonParser = new JsonParser();
		
		// read the contents of our json file and convert to JSONArray
		String jsonString = IOUtils.toString(fileInputStream);
		materials = new JSONArray(jsonString);
		
		return materials;
	}
	
	public static JSONArray getAllMaterialsFromFile(String filePath) throws Exception {
		FileInputStream inputStream = new FileInputStream(filePath);
		
		String jsonString = IOUtils.toString(inputStream);
		JSONArray materials = new JSONArray(jsonString);
		
		return materials;
	}
	
	public static JSONArray convertMaterialsForExporting(JSONArray materials) {
		// we do not want all of the properties from the json file to be exposed in the list
		JSONArray exportMaterials = new JSONArray();
		
		// loop over all materials
		// create new JSONObject only containing the properties in the exportingproperties array defined above
		for(int i=0; i<materials.length(); i++) {
			JSONObject material = materials.getJSONObject(i);
			JSONObject exportMaterial = new JSONObject();
			
			// only add properties we want
			Iterator<String> propertiesIterator = exportingProperties.iterator();
			while(propertiesIterator.hasNext()) {
				String property = propertiesIterator.next();
				exportMaterial.put(property, material.get(property));
			}
			
			exportMaterials.put(exportMaterial);
		}
		
		return exportMaterials;
	}
	
}
