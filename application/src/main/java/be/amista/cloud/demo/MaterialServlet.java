package be.amista.cloud.demo;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonParser;
import com.sap.cloud.sdk.cloudplatform.logging.CloudLoggerFactory;

import be.amista.cloud.helpers.MaterialHelper;

import org.apache.commons.io.IOUtils;
import org.json.*;

@WebServlet("/Materials")
public class MaterialServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = CloudLoggerFactory.getLogger(MaterialServlet.class);      

    @Override
    protected void doGet( final HttpServletRequest request, final HttpServletResponse response )
        throws ServletException, IOException
    {
    		// get filepath of the json file containing our dataset
    		ServletContext servletContext = getServletContext();
        String fullPath = servletContext.getRealPath("/WEB-INF/materials.json");

    		try {
    			//FileInputStream inputStream = new FileInputStream(fullPath);
    			//JSONArray materials = MaterialHelper.getAllMaterialsFromFileStream(inputStream);
    			JSONArray materials = MaterialHelper.getAllMaterialsFromFile(fullPath);
    			JSONArray exportMaterials = MaterialHelper.convertMaterialsForExporting(materials);
    			
    			response.getWriter().write(exportMaterials.toString());
            } catch (Exception e) {
                response.getWriter().write(e.getMessage());
            }
    }
}
